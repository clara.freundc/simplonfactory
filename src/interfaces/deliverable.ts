// Interfaces

export interface Deliverable {

    estimateDeliveryTime(weight: number): number;
    calculateShippingFee(weight: number): number;

}

// estimateDeliveryTime :
// < 10kg : retourne 7
// sinon retourne 10

// calculateShippingFee :
// < 1kg : retourne 5
// entre 1kg et 5kg : retourne 10
// > 5kg : retourne 20

export interface StandardDelivery extends Deliverable {

    

}

export interface ExpressDelivery extends Deliverable {

    

}