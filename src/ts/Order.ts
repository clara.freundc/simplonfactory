import { Customer, Address } from "./Customer";
import { Product } from "./Product";

export class Order {

    constructor(public orderId: number, public customer: Customer, public productList: [Product], public orderDate: Date) {
    }

    addProduct(product: Product) {
        // ajouter un produit dans le tableau de produits
        this.productList.push(product)
    }
    removeProduct(productId: number) {
        // On cherche l'index du produit en fonction de son productId, qui pourrait être différent de l'index
        const index = this.productList.findIndex(product => product.productId === productId);

        // On supprime le produit identifié
        this.productList.splice(index, 1)
    }
    calculateWeight() {
        // Calcul du poids total du tableau
        // On initialise le poids total 
        let totalWeight = 0;

        // pour chaque produit du tableau, on additionne la propriété weight
        for (const good of this.productList) {
            totalWeight += good.weight
        }
        return totalWeight
    }
    calculateTotal() {
        // Calcul du prix des éléments du tableau
        //On initialise un prix de base : 
        let totalPrice = 0;

        // pour chaque produit du tableau, on additionne le prix de chaque produit :
        for (const good of this.productList) {
            totalPrice += good.price
        }
        return totalPrice
    }
    // les informations de l'utilisateur, les informations de chaque produit et le total de la commande.
    displayOrder() {
        // Comme il y a beaucoup d'informations à donner et afficher sur plusieurs lignes, et des méthodes à appeler (pour accéder au prix et au poids) on peut faire de la manière suivante : 
        let orderDetails = "Summary of your order :\n"
        orderDetails += `Customer details :\n\n   Customer ID: ${this.customer.customerId}\n   Name: ${this.customer.name}\n   Email: ${this.customer.email}\n`

        // une erreur de type : Object is possibly 'undefined' s'affiche pour les lignes contenant this.customer.address.street donc, pour apaiser typescript, j'ai ajouté un if :
        // si this.customer.adress est truthy (donc pas undefined) alors il ajoute la ligne contenant l'addresse
        if (this.customer.address) {
            orderDetails += `   Shipment address : ${this.customer.address.street}, ${this.customer.address.zipCode} ${this.customer.address.city}, ${this.customer.address.country} \n\n`
        }
        orderDetails += `Order ID : ${this.orderId}\n`
        orderDetails += `Date : ${this.orderDate}\n`
        orderDetails += `You ordered: \n\n`
        for (const good of this.productList) {
            orderDetails += `   Name : ${good.name},\n   Product Id : ${good.productId}\n   Price : ${good.price} $\n   Weight : ${good.weight} kg\n\n`
        }
        orderDetails += `Total weight : ${this.calculateWeight()} kg.\n`
        orderDetails += `Total cost : ${this.calculateTotal()} $.\n`
        return orderDetails
    }
}