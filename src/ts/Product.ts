// Enum ClothingSize
export enum ClothingSize {
    XS = "XS",
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL"
}

// Enum ShoeSize
export enum ShoeSize {
    // On initialise la première valeur à 36, les autres sont automatiquement incrémentées de 1  (donc size37 = 37)
    size36 = 36,
    size37,
    size38,
    size39,
    size40,
    size41,
    size42,
    size43,
    size44,
    size45,
    size46
}

// Autre manière de définir un type
export class Dimensions {
    constructor(public length: number, public width: number, public height: number) {}
}

// On exporte et initalise la classe Product
export class Product {
    // Dans une version raccourcies du code, on précise dans le constructeur les propriétés publiques (qui peuvent être réutilisées en dehors de la classe)
    constructor(public productId: number, public name: string, public weight: number, public price: number, public dimensions: Dimensions) {

    }
    // Ici la méthode de la classe Product
    displayDetails(): string {
        // Elle permet d'afficher une chaîne de caractères avec les informations suivantes :
        // this.propriété appelle la propriété spécifique à l'istance de la classe
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Dimensions: length : ${this.dimensions.length}cm, width: ${this.dimensions.width}cm, height: ${this.dimensions.height} cm, Price: ${this.price} €.`
    }
}

export class Clothing extends Product {
    size: ClothingSize

    constructor (productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ClothingSize) {
        super(productId, name, weight, price, dimensions),
        this.size = size;
    }
    displayDetails(): string {
        return `Product ID: ${this.productId}, Name: ${this.name}, Size: ${this.size}, Weight: ${this.weight}, Price: ${this.price} €.`
    }
}

export class Shoes extends Product {
    size: ShoeSize

    constructor (productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ShoeSize) {
        super(productId, name, weight, price, dimensions),
        this.size = size;
    }
    displayDetails(): string {
        return `Product ID: ${this.productId}, Name: ${this.name}, Size: ${this.size}, Weight: ${this.weight}, Price: ${this.price} €.`
    }
}