// Type Address : street (texte), city (texte), postalCode (texte), country (texte).

// Ajouter la propritété address dans la classe Customer, de type Address | undefined.
// Inutile de modifier le constructeur de Customer.
// Ajouter la méthode setAddress(address: Address) : Renseigne l'adresse du client
// Ajouter la méthode displayAddress(): string : Retourne les informations de l'adresse au format : "Address: $street, $postalCode $city, $country" si l'adresse existe, ou "No address found" sinon.
// Modifier la méthode displayInfo() afin d'ajouter les informations de l'adresse.

// On utilise une interface pour la réutiliser comme type
export type Address = {
    street: string, 
    city: string, 
    zipCode: number, 
    country: string
}

export class Customer {
    // On initialise la variable address de type Address
    // le ? rend la valeur de la variable facultative 
    address?: Address

    constructor(public customerId: number, public name: string, public email: string) {
    }
    displayInfo(): string {
        return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}`
    }
    // Cette méthode permet de modifier la variable address
    setAddress(address: Address) {
        this.address = address
        // this.address = new Address("Sesame street", "New York", 10001, "USA");
    }
    displayAddress(): string {
        if (this.address !== undefined) {
            return `Address: ${this.address.street}, ${this.address.zipCode} ${this.address.city}, ${this.address.country}`
        } else {
            return `No address found.`
        }
    }
}