import { Customer } from "./Customer.js";
import { Product, Dimensions, Shoes, Clothing, ShoeSize, ClothingSize } from "./Product.js";
import { Order } from "./Order.js";


// Tu vas travailler pour SimplonFactory, une entreprise innovante dans le secteur de la fabrication et de la vente en ligne.
// Ta mission est de créer un système qui gère les commandes des clients, le catalogue de produits, et le processus de livraison. 
// Ce système devra permettre de créer et de gérer des clients (Customer), de gérer un inventaire de produits (Product) 
// avec des spécificités telles que les vêtements (Clothing) et les chaussures (Shoes), et de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).

// On crée une nouvelle instance pour la classe Dimensions, qui indiquera les dimensions de magazine
const magDimensions = new Dimensions(10, 4, 0.5);
const shoeDimension = new Dimensions(8, 5, 7);
const shirtDimension = new Dimensions(60, 40, 1);

// Nouvelles instances de Product
const magazine = new Product(1, "Weekly Shônen Jump", 1, 1.80, magDimensions)
// ShoeSize et ClothingSize sont appelées par leurs class
const shoe1 = new Shoes(1, "basket", 0.5, 10, shoeDimension, ShoeSize.size37)
const clothe1 = new Clothing(1, "t-shirt", 0.4, 10, shirtDimension, ClothingSize.M)

// Nouvelles instances de Customer
const customer = new Customer(1, "Georges", "georges.georgiest@georgius.com")

// Nouvelles instances de Address
let customerAddress = {
    street: "Sesame street", 
    city: "New York", 
    zipCode: 10001, 
    country: "USA"
}

// Nouvelles instances de Date
const today = new Date(2024, 4, 2)

// Nouvelles instances de Order
const order1 = new Order(1, customer, [shoe1], today)

console.log(customer.setAddress(customerAddress))
console.log(order1.displayOrder())